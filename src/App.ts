import * as express from 'express'
import * as db from './db';
import * as sequelize from 'sequelize'

class App {
  public express

  constructor () {
    this.express = express()
    this.mountRoutes()
  }

  private mountRoutes (): void {
    const router = express.Router()
    router.get('/', (req, res) => {
      res.json({
        message: 'Hello World!!'
      })
    });

    router.get('/push', (req, res) => {
      Project.findOne({ where: {name: 'test'} }).then(project => {
        console.log("found", project);
      });
      db.sequelize.query("SELECT * FROM testt", { type: sequelize.QueryTypes.SELECT, model: Project }).then(projects => {
        console.log(projects)
        res.json({
          message: projects
        })
      }).catch(error => {
        res.json({
          message: error
        })
      })
    })
    this.express.use('/', router)
  }
}

const Project = db.sequelize.define('testt', {
  id: {
    type: sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: sequelize.STRING,
    allowNull: true,
    get() {
      const id = this.getDataValue('id');
      // 'this' allows you to access attributes of the instance
      return this.getDataValue('name') + ' (' + id + ')';
    }
  }
}, {
  freezeTableName: true,
  timestamps: false
})

export default new App().express
